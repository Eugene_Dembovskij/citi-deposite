// Google Analytics tracking code
if ((window.location.href.indexOf('citibank.ru')>=0) || (window.location.href.indexOf('citibank.com')>=0)) {
	var _gaq = _gaq || [];
	_gaq.push(['_setAccount', 'UA-17370299-2']);
	_gaq.push(['_setDomainName', 'none']);
	_gaq.push(['_setAllowLinker', true]);
	_gaq.push(['_trackPageview']);

	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
}

// Yandex.Metrika tracking code
if ((window.location.href.indexOf('citibank.ru')>=0) || (window.location.href.indexOf('citibank.com')>=0)) {
	(function (d, w, c) {
		(w[c] = w[c] || []).push(function() {
			try {
				w.yaCounter10209925 = new Ya.Metrika({id:10209925,
				webvisor:true,
				clickmap:true,
				accurateTrackBounce:true});
			} catch(e) { }
		});
	
		var n = d.getElementsByTagName("script")[0],
				s = d.createElement("script"),
				f = function () { n.parentNode.insertBefore(s, n); };
		s.type = "text/javascript";
		s.async = true;
		s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";
	
		if (w.opera == "[object Opera]") {
			d.addEventListener("DOMContentLoaded", f, false);
		} else { f(); }
	})(document, window, "yandex_metrika_callbacks");
}

var formFieldReset = function(idFrom){
    $(idFrom).find('.has-error').removeClass('has-error');
    $(idFrom).find('.error').empty();
    $(idFrom)[0].reset();
};

var tabs = {
    init: function () {
        $(document).on('click', '.js-tabs__item', function () {
            var activeIndex = $(this).index();
            var tabs = $(this).closest('.js-tabs');
            var tabsList = tabs.find('.tabs__list').eq(0);
            // var siblings = $(this).siblings();
            var content = tabs.find('.js-tabs__content:first');

            if(!$(this).hasClass('is-active')){
                $(this).toggleClass('is-active').siblings().removeClass('is-active').find('.dh-overlay').removeAttr('style');
                content.children().eq(activeIndex).slideToggle(10, function(){
                    if($(this).find('.services-slider').hasClass('slick-initialized')){
                        $(this).find('.services-slider').slick("getSlick").refresh();
                    }
                }).siblings().hide();
                var $heigh2= $(".article__content").offset().top + 30;
                $('html, body').animate({scrollTop: $heigh2}, 300, function(){
                    $('.terms').show();
                    $('.terms__content-scroll.scrollbar').getNiceScroll().resize();
                });
                if(!$('.tabs__list > div').hasClass('is-active')){$('.terms').hide();}
                $('.offers h4, .offers h4 + span, .offers__wrap, .offers__list').addClass('fadeInUp animated');
                formFieldReset('#cgclient');
            }

            if(activeIndex == 0){
                hashSwap('prioritybanking');
            }

            if(activeIndex == 1){
                hashSwap('goldbundle');
            }

            if(activeIndex == 1){
                $('.feedback, .modal').addClass('clr-gold');
            } else {
                $('.feedback, .modal').removeClass('clr-gold');
            }
        });
    }
}

var servicesSlider = {
    slider: $('.services-slider'),
    sliderSettings: function () {
        return {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: false,
            focusOnSelect: false,
            arrows: false,
            adaptiveHeight: true,
            dots: true,
            speed: 500
        }
    },
    init: function () {
        if ($('.services-slider').length && $(window).width() < 768) {
            servicesSlider.slider.slick(servicesSlider.sliderSettings());
        }
        $(window).on('resize', function () {
            if ($(window).width() >= 768) {
                if (servicesSlider.slider.hasClass('slick-initialized')) {
                    servicesSlider.slider.slick('unslick');
                }
            } else {
                if (!servicesSlider.slider.hasClass('slick-initialized')) {
                    servicesSlider.slider.slick(servicesSlider.sliderSettings());
                }
            }
        });
    }
};

var servicesScroller = {
    init: function(){
        if($('.offers__list.scrollbar').length && $(window).width() < 768) {
            $('.offers__list.scrollbar').scroller();
        }

        $(window).on('resize', function () {
            if ($(window).width() >= 768) {
                if($('.offers__list.scrollbar').hasClass('scroller')){
                    $('.offers__list.scrollbar').scroller('destroy');
                }
            }
        });
    }
};

var fixedTabs = {
    thisScroll: null,
    heightMenu: null,
    heightMenuTmp: null,
    sectionTarget: null,
    scrollYnav: null,
    winheight: null,
    init: function(){
        fixedTabs.elemOffsetTop();
        fixedTabs.scrollDetect();
        fixedTabs.winheight = $(window).height();

        $(document).on('scroll', function(e){
            fixedTabs.scrollDetect();
            fixedTabs.animateElems('.animateblock');
        });
    },
    elemOffsetTop: function(){
        fixedTabs.heightMenu = ($('.is-fixed.tabs__list-wrap').outerHeight(true) > 0) ? 0 : $('.tabs__list-wrap').outerHeight(true);
        if($('.tabs__list-wrap').outerHeight(true) > 0 && $('.tabs__list-wrap').hasClass('is-fixed')){
            fixedTabs.heightMenuTmp = 0;
        } else {
            fixedTabs.heightMenuTmp = $('.tabs__list-wrap').outerHeight(true);
        }

        fixedTabs.scrollYnav = Math.floor($('.article__content').offset().top);
    },

    scrollDetect: function(){
        fixedTabs.thisScroll = $(window).scrollTop();
        if(fixedTabs.thisScroll > fixedTabs.scrollYnav + ((fixedTabs.heightMenuTmp > 0) ? 0 : fixedTabs.heightMenu)) {
            if(!$('.tabs__list-wrap').hasClass('is-fixed')){
                $('.tabs__list-wrap').addClass('is-fixed');
                $('.article__header').css('padding-bottom', fixedTabs.heightMenu + 'px');
                if($(window).width() < 768){
                    $('.offers').css('padding-top', fixedTabs.heightMenu + 'px');
                }
            }
        }

        if(fixedTabs.thisScroll < fixedTabs.scrollYnav + ((fixedTabs.heightMenuTmp > 0) ? 0 : fixedTabs.heightMenu)){
            if($('.tabs__list-wrap').hasClass('is-fixed')){
                $('.tabs__list-wrap').removeClass('is-fixed');
                $('.article__header').removeAttr('style');
                if($(window).width() < 768){
                    $('.offers').removeAttr('style');
                }
            }
        }
    },

    animateElems: function(elems){
        fixedTabs.thisScroll = $(window).scrollTop();
        var $elems = $(elems);
 
        $elems.each(function(){
            var $elm = $(this), topcoords;
            if($elm.hasClass('fadeInUp animated')) { return true; }
            topcoords = $elm.offset().top;
            
            if(fixedTabs.thisScroll > (topcoords - (fixedTabs.winheight * 0.75))) {
                $elm.addClass('fadeInUp animated');
            }
        });
    }
};

var termsScroller = {
    scroll: null,
    init: function(){
        termsScroller.scroll = $('.terms__content-scroll.scrollbar').niceScroll({
            cursorwidth: ($(window).width > 767) ? "8px" : "5px",
            touchbehavior: true,
            autohidemode: false,
            cursorcolor: "#B3B3B3",
            cursorborder: "",
            background:"#E6E6E6",
            cursorborderradius: 0,
            horizrailenabled: false,
            // zindex: 1,
        });
        termsScroller.scroll.scrollend(function(data){
            if (data.end.y >= this.page.maxh) {
                $('.terms__content').addClass('end');
            } else {
                $('.terms__content').removeClass('end');
            }
        });
    }
};

var hashSwap = function(target){
    var hashObj = $('#' + target);
    hashObj.addClass('active');
    tempHash = target;
    hashObj.attr('ID', target + '_temphash');
    window.location.hash = target;
    hashObj.attr('ID', target);
};

$(function(){

    if($('.hero-slider').length){
        $('.hero-slider').cdSlider();
    }

    if($('.tabs__list-wrap').length){
        fixedTabs.init();
    }

    tabs.init();
    servicesSlider.init();

    // servicesScroller.init();
    termsScroller.init();

    $('.dh-container').directionalHover({speed: 400});

    // $('.counter').counterUp({delay: 10,time: 300});

    $(document).on('click', '.aniscroll', function(e){
        var $heigh1 = $(".topanimate").offset().top;
        $('html, body').animate({scrollTop: $heigh1}, 300);
    });

    $('.priorityINVTD').click(function(){
        setTimeout(function(){
            $('.feedback').show();
            $('.terms__content-scroll.scrollbar').getNiceScroll().resize();
            var $heigh1 = $(".feedback").offset().top - $('.tabs__list-wrap.is-fixed').outerHeight(true);
            $('html, body').animate({scrollTop: $heigh1}, 300);
        });
    });

    $('.GoldINVTD').click(function(){
        setTimeout(function(){
            $('.feedback').show();
            $('.terms__content-scroll.scrollbar').getNiceScroll().resize();
            var $heigh1 = $(".feedback").offset().top - $('.tabs__list-wrap.is-fixed').outerHeight(true);
            $('html, body').animate({scrollTop: $heigh1}, 300);
        });
    });
    

    $(document).on('click', '.link1', function(){
        $('#prioritybanking').hide();
        $('.feedback').show();
        $('.tabs__list > div:first-child').trigger('click');
    });
    
    $(document).on('click', '.link2', function(){
        $('#goldbundle').hide();
        $('.feedback').show();
        $('.tabs__list > div:last-child').trigger('click');
    });
    
    $(window).on('load', function(){
        var urlHash = window.location.hash;
        if(urlHash == "#prioritybanking"){
            $('.tabs__list > div:first-child').trigger('click');
        }

        if(urlHash == "#goldbundle"){
            $('.tabs__list > div:last-child').trigger('click');
        }
    });
});