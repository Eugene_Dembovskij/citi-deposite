Popup = {
    hideModal: function(id){
        if($('.collapse.in').is(':visible')){
            $(id).collapse('hide');
            $('.modal-backdrop').fadeOut(function() {
                this.remove();
            });
            $(id).collapse('hide');
            $(id).find('.user-name').empty();
            $('body').removeClass('modal-open');
        }
    },
    shownModal: function(id, user){
        $('body').addClass('modal-open');
        if($('#navbar').is(':visible')) $('#navbar').collapse('hide');
        $(id).find('.user-name').text(user);
        $(id).collapse('show').add($(id).children('.modal__dialog').css({
            "top": (($(window).height() - $(id).children('.modal__dialog').outerHeight(true)) / 2 < 10) ? '10px' : ($(window).height() - $(id).children('.modal__dialog').outerHeight(true)) / 2 + 'px',
            "left": '0',
            "right": '0',
            "position": 'absolute',
            "z-index": '1020'
        })).before('<div class="modal-backdrop fade"></div>').add($('.modal-backdrop').css('background-color', '#ffffff').addClass('in').css('height', '110%'));
        
        if(/iPhone|iPod|iPad/i.test(navigator.userAgent)) {
            $('.modal').addClass('modal_iosfix');
            $('.modal-backdrop').addClass('modal_iosfix');
        }
        
        $(document).on('click.bs.collapse', '.modal .modal__close', function() {
            Popup.hideModal(id);
        });
    }
};

Form = {
    $Form: null,
    FocusValue: 1,
    params: {},
    trimSpace: function (x) {
        var emptySpace = / /g;
        var trimAfter = x.replace(emptySpace, "");
        return (trimAfter);
    },

    validation: function (e) {
        var $this = $(e.currentTarget);
        var data = '';
        var isError = false;
        if (e.data.jForm) {
            Form.$Form = e.data.jForm;
        }
        var tmp, tmpName;

        Form.$Form.find('.js-requere').each(function () {
            tmp = $(this);
            tmpName = tmp.attr('name');
            if (tmpName) {
                Form.params[tmpName] = tmp.val();
            }

            if (tmpName == 'Fname') {
                if (Form.trimSpace(tmp.val()) == '') {
                    if (tmp.val() == '	') {
                        tmp.val() = '';
                    }
                    tmp.parent().removeClass('has-complete').addClass('has-error');
                    isError = true;
                    return false;
                }

                if (tmp.val().length < 3) {
                    tmp.parent().removeClass('has-complete').addClass('has-error');
                    isError = true;
                    return false;
                }

                tmp.parent().removeClass('has-error');
                isError = false;
            }

            if (tmpName == 'phonenum11') {
                if (Form.trimSpace(tmp.val()) == '') {
                    tmp.parent().removeClass('has-complete').addClass('has-error');
                    isError = true;
                    return false;
                }
                tmp.parent().removeClass('has-error');
                isError = false;
            }

            tmp.parent().addClass('has-complete');

        });

        if (isError) {
            Form.$Form.find('.has-error').each(function () {
                if ($(this).hasClass('has-error')) {
                    $('html, body').animate({
                        scrollTop: $(this).find('.js-requere').offset().top - $('.tabs__list-wrap').outerHeight(true)
                    }, 'slow');
                    return false;
                }
            });
        } else {
            var data = {
                fname: Form.params.Fname,
                phonenum11: Form.params.phonenum11
            };
            var path = window.location.pathname.split("/");

            // console.log('data= ', data);
            $.post(Form.$Form.attr('action'), data, function (json) {
                //if(json.success){}
            }, "json");

            if (path.length > 1) {
                var matches = window.location.hash;
                if (matches == "#prioritybanking") {
                    // analitics('event13', ';Citigold', '1464333941135', '1464333941135');
                    yaCounter10209925.reachGoal('Deposit_CP');
                    ga('send', 'event', 'Click', 'Form', 'Deposit_CP');
                }
                if (matches == "#goldbundle") {
                    // analitics('event13', ';Citigold', '1464333941135', '1464333941135');
                    yaCounter10209925.reachGoal('Deposit_CG');
                    ga('send', 'event', 'Click', 'Form', 'Deposit_CG');
                }
            }

            Popup.shownModal('#popup-success', data.fname);
        }
    },

    init: function (elem) {
        var phone = $(elem).find('input[name="phonenum11"]');
        if (phone.length) {
            phone.mask('+9(999)999-99-99');
        }
        $('#submit').on('click', {
            jForm: $(elem)
        }, Form.validation);
    }
};
$(function () {
    Form.init('#cgclient');
});